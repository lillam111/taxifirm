// everything in this particular file will be included prior to everything else being included... anything that requires
// to be used across the entire application set will be included here.

// file system
const fs = require('fs');

// inclusion of jquery
window.$ = window.jQuery = require('jquery');

// inclusion of electron database
const db = require('electron-db');
const path = require('path');
const db_path = path.join(__dirname, '/lib/db');
const db_archive_path = path.join(__dirname, '/lib/db/archives');
const db_increment_path = path.join(__dirname, '/lib/db/increments');

// PDF creation
const PDFDocument = require('pdfkit');
const pdf_exports_path = path.join(__dirname, 'exports');

// inclusion of jsonQuery - better functionality for quering the database.
const jsonQuery = require('json-query');

// environmental variables.
const config = require('dotenv').config();

// datetime stuff
const moment = require('moment');