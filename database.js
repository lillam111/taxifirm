// make the directory for database, if it doesn't exist... (this will be the initiation of the first time setup...)
// this is also going to be setting the application up with the possibility of having archives, meaning the the user
// doesn't ever have to remove the files... but archive them instead.
if (! fs.existsSync(db_path))           fs.mkdirSync(db_path);
if (! fs.existsSync(db_archive_path))   fs.mkdirSync(db_archive_path);
if (! fs.existsSync(db_increment_path)) fs.mkdirSync(db_increment_path);

// define the tables that we are going to be using throughout the application; if more tables gets placed in with
// this... then they will be created on the next time the application loads.
let tables = [
	{ table: 'bookings',               archives: false },
	{ table: 'bookings_users',         archives: false },
	{ table: 'regular_bookings',       archives: false },
	{ table: 'vehicles',               archives: false },
	{ table: 'drivers',                archives: false },
	// { table: 'plots',               archives: false  },
];

tables.forEach((table, key) => {
	if (! db.tableExists(table.table, db_path)) {
		db.createTable(table.table, db_path, (success, message) => {});
	}

	if (! db.tableExists(table.table, db_increment_path)) {
		db.createTable(table.table, db_increment_path, (success, message) => {});
		db.insertTableContent(table.table, db_increment_path, { _id: 1, increment: 0 }, (success, message) => {});
	}

	if (table.archives === true && ! db.tableExists(`${table.table}`, db_archive_path)) {
		db.createTable(`${table.table}`, db_archive_path, (success, message) => {});
	}	
});

/**
* all of this wants moving into it's own javascript file (database.js)... so this can be all localised into one set
* incredible orientation of data... for handling the ordering filtration and whatnot of the database at hand...
*
* @param table
* @param options
* @param direction
* @returns {*}
*/
const get = function (table, options = {}, direction = 'asc') {
	window.temp_data = [];

	var table_data = [];

	db.getRows(table, db_path, options, (message, data) => {
		window.temp_data = data;
	});

	if (typeof (window.temp_data) === "object") {
		table_data = direction === 'asc' ? window.temp_data.reverse() : window.temp_data;
	}

	// unset this from memory, not sure if this is being stored in memory too much, but every time we get we want
	// to remove it entirely.
	delete window.temp_data;

	return table_data;
};

/**
* this method for collecting the archives of a specified table, this method is so that the developer doesn't have to
* specify anywhere in the get method or the full table name and get literally just write getArchives('bookings') and
* will fetch the archives of the passed model.
*
* @param table
* @param options
* @param direction
* @returns {Array|*}
*/
const getArchives = function (table, options = {}, direction = 'asc') {
	window.temp_data = [];

	var table_data = [];

	db.getRows(table, db_archive_path, options, (message, data) => {
		window.temp_data = data;
	});

	if (typeof (window.temp_data) === "object") {
		table_data = direction === 'asc' ? window.temp_data.reverse() : window.temp_data;
	}

	// unset this from memory, not sure if this is being stored in memory too much, but every time we get we want
	// to remove it entirely.
	delete window.temp_data;

	return table_data;
};

/**
* This method is simply for incrementing a very particular column on the incremenets/db_name so I have an idea of what
* the last id of the entered item is... This is simply going to be taking care of the updating of the one column, and
* return the column back so the entry we are attempting to save into the database will be incremented the column id as
* well... This will be incremented and never decremented... so the id will always be something new; rather than having
* the potentially of being the same... this escapes the issue with the automatic id that electron-db is giving me...
* a programmatic flaw was causing me to get more than 1 entry for a unique identifier...
*
* @param table
* @returns {number}
*/
const increment = function (table) {
	db.getField(table, db_increment_path, 'increment', (message, data) => { window.current_increment = data.first(); });
	var increments = window.current_increment += 1;
	// update the row with the new increment, this is the new _id of every single table that isn't this particular
	// method of handling.
	db.updateRow(table, db_increment_path, { _id: 1 }, { increment: increments }, (success, message) => {});
	delete window.current_increment;
	return increments;
};

/**
* this method for saving data, if we are passing through options... then we are going to be working with an update query,
* otherwise, if we have not passed in any arguments in the options, we are looking directly at a new element all
* together, thus we are going to insert new table content.
*
* @param table
* @param options
* @param data
*/
const save = function (table, options = null, data = {}) {
	if (options === null) {
		var extra = { _id: increment(table) };
		db.insertTableContent(table, db_path, { ...data, ...extra }, (success, message) => {});
		return;
	} db.updateRow(table, db_path, options, data, (success, message) => {});
};

/**
* this method is for removing rows from the database. we are going to be passing table, the table of which wants
* something deleting from, options which will contain clauses on what we're deleting; multiple things can be passed
* through, id, name, date. this makes deleting elements from the database much easier and more universalised. if
* archive gets passed as a true, then the system will remove the element from the live database and put it into the
* archived directory that will have been reated on the loading of the application.
*
* @param table
* @param options
* @param to_archive
* @param callback
*/
const del = function (table, options = {}, to_archive = false, callback = null) {
	if (to_archive === true) archive(table, options);
	db.deleteRow(table, db_path, options, (success, message) => {}); 	

	if (callback !== null) {
		callback();
	}
};

/**
* this method will be for when deleting - opting to whether the user wants to archive the data or not. this will be
* useful for being able to clear the application and making it faster in places where they are no longer needing
* specific data in the system. However wanting to retain that data for further use later on.
*
* @param table
* @param options
*/
const archive = function (table, options = {}) {
	var to_archive = get(table, options);
	if (to_archive.length === 1 || typeof (to_archive.length) === 'undefined') {
		db.insertTableContent(`${table}`, db_archive_path, to_archive, (success, message) => {});
		return;
	} to_archive.forEach((archive, key) => {
		db.insertTableContent(`${table}`, db_archive_path, archive, (success, message) => {});
	});
};

/**
* this method is a dangerous method, especially in production, this method will essentially clear the table that has
* been passed through; all elements inside the json array file will be removed in it's entirety... this should only be
* used if you are wanting to wipe the application alltogether.
*
* @param table
* @param options
* @param to_archive
*/
const clear = function (table, options = {}, to_archive = false) {
	if (typeof (table) === 'object') {
		table.forEach(function (table, key) {
			db.clearTable(table.table, db_path, (success, message) => {});
			if (table.archives === true) {
				db.clearTable(`archived_${table.table}`, db_archive_path, (success, message) => {});
			}
		}); return;
	} db.clearTable(table, db_path, (success, message) => {});
};

/**

to do: 

1 better visibility of the regular bookings (show everything in by a month by month basis)
2 when putting a booking in, the system only shows bookings for the precise time, and not for the whole day
3. editable fields randomly become non clickable (this is system wide).
4. need to be able to mark a job off as done.

*/