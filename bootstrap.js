// Setting up the appropriate constants for this application; when this app boots up we are going to define the app as
// a global constant that should be re-useable across the entire application.
const { 
	app, 
	BrowserWindow,
	ipcMain
} = require('electron');

let win;

const { autoUpdater } = require('electron-updater');

/**
* create window function, the defualt parameter to this will be the index.html, if we don't pass in a file, the
* referenced file will always be index.html.
*
* @param file
* @param the_document
*/
function create_window(file = 'main.html', the_document) {
	var show = process.platform !== 'darwin' ?
		false : true;

	win = new BrowserWindow({
		webPreferences: {
			nodeIntegration: true
		},
		show: show
	});

	// load the passed file.
	win.loadFile(file);
	// win.removeMenu();
	win.maximize();

	// if we desire to see the web tools... uncomment this line out if you are going to want to be seeing the developer
	// console tool across this application.
	win.webContents.openDevTools();
}

// when the application is ready, we are going to be wanting to show the create window function
app.whenReady().then(create_window);

app.on('activate', () => {
	if (BrowserWindow.getAllWindows().length === 0) {
		create_window('index.html', document);
	}
});

// autoUpdater.on('update-available', (info) => {
// 	alert(`Update is available Version: ${info.version} Release date: ${info.releaseDate}`);
// });

// autoUpdater.on('update-not-available', () => {
// 	alert('an update is not availalbe');
// });

// autoUpdater.on('download-progress', (progress) => {
// 	console.log(`Progress: ${Math.floor(progress.percent)}`);
// });

// autoUpdater.on('update-downloaded', (info) => {
// 	alert('Update Downloaded');
// 	autoUpdater.quitAndInstall();
// });

// autoUpdater.on('error', (error) => {
// 	alert(error);
// });

// app.on('ready', () => {
// 	autoUpdater.checkForUpdates();
// });

// // when the update has downloaded, send updateready and then force the application to quit and install.
// autoUpdater.on('update-downloaded', (event) => {
// 	confirm('An update is available, Application will restart');
// 	win.webContents.send('updateReady');
// 	autoUpdater.quitAndInstall();
// });

// when all the application pages has been closed... we are  going to want to execute the app.quit which will close
// the application down and stop all processes for this project instance.
app.on('window-all-closed', () => { 
	if (process.platform !== 'darwin') {
		app.quit();
	}
});