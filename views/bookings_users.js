window.$(() => {
    // when clicking on the header navigation or booking_user_back, we are going to call the method
    // view_booking_users() so that the table data for bookings will display on the page.
    window.$('body').on('click', '.header-navigation.view_bookings_users, .bookings_user_back', (event) => {
        start_loader();
        setTimeout(function () {
            view_booking_users().then(() => {
                end_loader();
            });
        }, 50);
    });

    // when clicking on booking_view element in the table, we are going to call the view_booking() method, pass in the
    // id of the element we have just clicked on so that we can see that booking in detail.
    window.$('body').on('click', '.bookings_users_view', (event) => {
        start_loader();
        setTimeout(function () {
            view_booking_user(window.$(event.target).closest('tr').attr('data-id')).then(() => {
                end_loader();
            });
        }, 50);
    });

    // when we click the add booking button, we are going to be loading a view for the viewing of a booking - this will
    // literally just transform the html page and enter new content into the $('#body') html tag... no id will be passed
    // so that we know that we aren't going to be working with an existing booking...
    window.$('body').on('click', '.bookings_users_add', (event) => {
        start_loader();
        setTimeout(() => {
            view_booking_user(null).then(() => {
                end_loader();
            });
        }, 50);
    });

    // when clicking on bookings_user_view element in the table, we are going to call the
    // view_booking_user() method, pass in the id of the element we have just clicked on so that we can see that
    // booking in detail.
    window.$('body').on('click', '.bookings_user_view', (event) => {
        start_loader();
        setTimeout(function () {
            view_booking_user(window.$(event.target).closest('tr').attr('data-id')).then(() => {
                end_loader();
            });
        }, 50);
    });

    // when the user clicks on booking_save element... we are going to be finding the form element (which will be
    // assigned to a div grid element in the stack (for formatting purposes)) this method will be getting the fields
    // from the get fields method of this stack; and iterating over them to find the corresponding element in the stack.
    // the element will be booking_fieldname and this offers some sense of simplistic nature to the overall process.
    window.$('body').on('click', '.bookings_user_save', (event) => {
        let $this = window.$(event.target),
            $form = $this.closest('.form'),
            booking_user_id = (
                typeof($this.attr('data-id')) !== "undefined" && $this.attr('data-id') !== ''
            ) ? { _id: parseInt($this.attr('data-id')) } : null;

        var booking_user_data = validate_form(
            'bookings_user',
            $form,
            get_booking_user_fields()
        );

        if (! booking_user_data.errors) {
            // save the booking, the id will either be passed through as an object id: booking_id or will be passed as
            // null. which means that this will either be a save feature, or an edit feature. (this is combining the
            // functionality make and edit).
            save('bookings_users', booking_user_id, booking_user_data.data);

            // after we have inserted a new booking, we are going to want to view the bookings again; so that this will
            // be re-injected into the listing upon creation.
            start_loader();
            setTimeout(() => {
                view_booking_users().then(() => {
                    end_loader();
                })
            }, 50);
        }
    });

    // this functionality is in place so that the user is going to be able to enter some search term into the field,
    // press enter and have the booking users come back with filtered results...
    window.$('body').on('keydown', '.bookings_users_search', (event) => {
        if (event.key === 'Enter') {
            start_loader();
            setTimeout(() => {
                view_booking_users(window.$(event.target).val())
                    .then(() => {
                        end_loader();
                    });
            }, 50);
        }
    });

    // functionality for being able to toggle select and unselect all checkbox items... this will
    // be for ease of deleting entry items.
    window.$('body').on('click', '.bookings_users_select_all', (event) => {
        let $this = window.$(event.target);
        $this.closest('table').find('.bookings_users_select').prop('checked', $this.prop('checked'));
        // if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
        // currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
        // so that the user will be able to make global deletions
        window.$('.bookings_users_options').find('.bulk_delete_bookings_users').addClass('uk-hidden');
        if (window.$('.bookings_users_select:checked').length > 0) {
            window.$('.bookings_users_options').find('.uk-hidden').removeClass('uk-hidden');
        }
    });

    // functionality for when clicking on the selections, and will continuously keep checking the length
    // of the currently existing checked checkboxes to see whether or not the overall checkbox (select all)
    // wants deselecting because nothing is selected...
    window.$('body').on('click', '.bookings_users_select', (event) => {
        if (window.$('.bookings_users_select:checked').length === 0) {
            window.$('.bookings_users_select').prop('checked', false);
        }
        // if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
        // currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
        // so that the user will be able to make global deletions
        window.$('.bookings_users_options').find('.bulk_delete_bookings_users').addClass('uk-hidden');
        if (window.$('.bookings_users_select:checked').length > 0) {
            window.$('.bookings_users_options').find('.uk-hidden').removeClass('uk-hidden');
        }
    });

    // functionality for being able to bulk select a bunch of rows, and then decide that you are wanting to bulk delete
    // a select number of rows. this functionality will iterate over all the selected checkbox items and then find
    // that checkbox row and take the data-attr('id') and utilise that as the finding for the id that we are going to
    // remove from the database.
    window.$('body').on('click', '.bulk_delete_bookings_users', (event) => {
        if (confirm('Are you sure you want to delete these booking users?')) {
            var $results = window.$('#body').find('#results');
            window.$('.bookings_users_select:checked').each((key,bookings_user_select) => {
                var number_of_results = parseInt($results.html().trim()),
                    $bookings_user_select = window.$(bookings_user_select),
                    $row = $bookings_user_select.closest('tr');
                del('bookings_users', { _id: parseInt($row.attr('data-id')) });
                $row.remove();
                $results.html(number_of_results - 1);
            }, $results);
            window.$(event.target).addClass('uk-hidden');
            if (window.$('.bookings_users_select:checked').length === 0) {
                window.$('.bookings_users_select_all').prop('checked', false);
            }
        }
    });
});

/**
*
* @returns {*[]}
*/
var get_booking_user_fields = function () {
    return [
        {
            field: 'name',
            placeholder: 'Enter a name for the user...',
            required: true,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'house_number',
            placeholder: 'Enter a house/flat number or building reference...',
            required: true,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'street',
            placeholder: 'Enter a street name...',
            required: true,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'county',
            placeholder: 'Enter a county...',
            required: true,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'postcode',
            placeholder: 'Enter a postcode for this user...',
            required: true,
            type: 'text',
            cast: 'string'
        }
    ]
};

/**
* This method is for showing all of the booking users in the system, the parameter of search value will be by
* default null and if the value isn't null, then we are going to filter out the elements that don't sit inside those
* items; this potentially is going to be a slow feature. Optimisations needed. This method will return the overall
* bookings in the system.
*
* @param search
*/
var view_booking_users = async function(search = null) {
    var booking_users = get('bookings_users').sort((booking_user_1, booking_user_2) => {
            if (booking_user_1.name < booking_user_2.name) return -1;
            if (booking_user_1.name > booking_user_2.name) return 1;
        }),
        html = '';

    // if the user has searched for something, then we are going to want to filter out some entries that reside within
    // the entries that we have returned from the json array... this might take some time when the system gets larger
    // depending on how big the system gets... then the user may take it upon themselves to clear out older data...
    if (search !== null) {
        booking_users = booking_users.filter(booking_user =>
            (typeof (booking_user.name) !== "undefined" &&
                booking_user.name.toLowerCase().includes(search.toLowerCase())) ||
            (typeof (booking_user.street) !== "undefined" &&
                booking_user.street.toLowerCase().includes(search.toLowerCase())) ||
            (typeof (booking_user.county) !== "undefined" &&
                booking_user.county.toLowerCase().includes(search.toLowerCase())) ||
            (typeof (booking_user.postcode) !== "undefined" &&
                booking_user.postcode.toLowerCase().includes(search.toLowerCase()))
        );
    }

    html += '<div id="booking_users" class="box-wrapper">';
    html += '<div class="uk-flex">';
        html += '<div class="uk-flex-auto">';
            html += '<div class="bookings_users_options">';
                html += '<a class="bulk_delete_bookings_users uk-button uk-button-small uk-button-danger uk-hidden"><i class="fa fa-trash"></i>Delete Selected</a>';
            html += '</div>';
        html += '</div>';
        html += '<div class="uk-flex-expand">';
            html += '<input type="text" class="search bookings_users_search" placeholder="Search..." value="' + (search !== null ? search : '') + '" />';
                html += '<a class="add bookings_users_add"><i class="fa fa-plus"></i></a>';
            html += '</div>';
        html += '</div>';
        var fields = ['name', 'street', 'county', 'postcode'];
        html += create_table('bookings_user', fields, booking_users);
    html += '</div>';

    window.$('#body').html(html);
    window.$('#title').html(': Booking Users');
    window.$('.header-navigation.active').removeClass('active');
    window.$('.header-navigation.view_bookings_users').addClass('active');

    // if the search value has been specified then we are going to focus the search bar, so that the user can alter
    // the search parameters if they choose to do so.
    if (search !== null) window.$('.booking_user_search').focus();
};

/**
*
* @param id
* @returns {Promise<void>}
*/
var view_booking_user = async function (id = null) {
    var booking_user = get('bookings_users', { _id: parseInt(id) }).first(),
        html = create_form('bookings_user', get_booking_user_fields(), booking_user);
    window.$('#title').html(': Booking User: ' + print(`${booking_user.name}`, ' Create new booking user'));
    window.$('#body').html(html);
};