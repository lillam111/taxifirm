window.$(() => {
	// when clicking on the header navigation or bookings_back, we are going to call the method view_regular_bookings()
	// so that the table data for bookings will display on the page.
	window.$('body').on('click', '.header-navigation.view_regular_bookings, .regular_booking_back', (event) => {
	    start_loader();
	    setTimeout(() => {
            view_regular_bookings().then(() => {
                end_loader();
            });
        }, 50);
	});

    // when clicking on booking_view element in the table, we are going to call the view_regular_booking() method, pass
	// in the id of the element we have just clicked on so that we can see that booking in detail.
    window.$('body').on('click', '.regular_booking_view', (event) => {
        start_loader();
        setTimeout(() => {
            view_regular_booking(window.$(event.target).closest('tr').attr('data-id'))
                .then(() => {
                    end_loader();
                });
        }, 50);
    });

    // when we click the add regular booking button, we are going to be loading a view for the viewing of a booking -
	// this will literally just transform the html page and enter new content into the $('#body') html tag... no id will
	// be passed so that we know that we aren't going to be working with an existing booking...
    window.$('body').on('click', '.regular_booking_add', (event) => {
        start_loader();
        setTimeout(() => {
            view_regular_booking(null).then(() => {
                end_loader();
            });
        }, 50);
    });

    // this functionality is in place so that the user is going to be able to enter some search term into the field,
    // press enter and have the regular bookings come back with filtered results...
    window.$('body').on('keydown', '.regular_bookings_search', (event) => {
        if (event.key === 'Enter') {
            start_loader();
            setTimeout(() => {
                view_regular_bookings(window.$(event.target).val()).then(() => {
                    end_loader();
                });
            }, 50);
        }
    });

    // functionality for being able to toggle select and unselect all checkbox items... this will be for ease of
    // deleting entry items.
    window.$('body').on('click', '.regular_bookings_select_all', (event) => {
        let $this = window.$(event.target);
        $this.closest('table').find('.regular_bookings_select').prop('checked', $this.prop('checked'));
        // if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
        // currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
        // so that the user will be able to make global deletions
        window.$('.regular_bookings_options').find('.bulk_delete_regular_bookings').addClass('uk-hidden');
        if (window.$('.regular_bookings_select:checked').length > 0) {
            window.$('.regular_bookings_options').find('.uk-hidden').removeClass('uk-hidden');
        }
    });

    // functionality for when clicking on the selections, and will continuously keep checking the length of the currently
    // existing checked checkboxes to see whether or not the overall checkbox (select all) wants deselecting because
    // nothing is selected...
    window.$('body').on('click', '.regular_bookings_select', (event) => {
        if (window.$('.regular_bookings_select:checked').length === 0) {
            window.$('.regular_bookings_select_all').prop('checked', false);
        }
        // if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
        // currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
        // so that the user will be able to make global deletions
        window.$('.regular_bookings_options').find('.bulk_delete_regular_bookings').addClass('uk-hidden');
        if (window.$('.regular_bookings_select:checked').length > 0) {
            window.$('.regular_bookings_options').find('.uk-hidden').removeClass('uk-hidden');
        }
    });

    // when the user clicks on regular_booking_save element... we are going to be finding the form element (which will
	// be assigned to a div grid element in the stack (for formatting purposes)) this method will be getting the fields
    // from the get fields method of this stack; and iterating over them to find the corresponding element in the stack.
    // the element will be regular_booking_fieldname and this offers some sense of simplistic nature to the overall
	// process.
    window.$('body').on('click', '.regular_booking_save', (event) => {
        let $this = window.$(event.target),
            $form = $this.closest('.form'),
            regular_booking_id = (
                typeof($this.attr('data-id')) !== "undefined" && $this.attr('data-id') !== ''
            ) ? { _id: parseInt($this.attr('data-id')) } : null;

        var regular_booking_data = validate_form('regular_booking', $form, get_regular_bookings_fields());

        if (! regular_booking_data.errors) {
            // save the booking, the id will either be passed through as an object id: booking_id or will be passed as
            // null. which means that this will either be a save feature, or an edit feautre. (this is cominbing the
            // functionality make and edit).
            save('regular_bookings', regular_booking_id, regular_booking_data.data);

            // after we have inserted a new booking, we are going to want to view the bookings again; so that this will
            // be re-injected into the listing upon creation.
            start_loader();
            setTimeout(() => {
                view_regular_bookings().then(() => {
                    end_loader();
                });
            }, 50);
        }
    });

    // this method, is when we are deciding the create all the bookings... and iterating over them all... if the user
    // clicks on this, the system will go through all regular bookings in the system and look to generate their rota
    // for one week specifically. (this is systematically designed to only work from week to week).
    window.$('body').on('click', '.make_bookings_from_regulars', (event) => {
        start_loader();
        setTimeout(() => {
            create_regular_bookings().then(() => {
                end_loader();
            });
        }, 50);
    });

    // this method is when we are deciding to create bookings from one regular booking entry, we will pass in the id of
    // that regular booking; and then iterate over all the days that are existing for that regular booking entry
    // specifically.
    window.$('body').on('click', '.run_regular_bookings_create', (event) => {
        start_loader();
        setTimeout(() => {
            create_regular_bookings(window.$(event.target).closest('tr').attr('data-id'))
                .then(() => {
                    end_loader();
                });
        }, 50);
    });

    // functionality for being able to delete the entry itself. this method on click of body, will latch this event onto
    // any class that has regular_booking_delete... and will find the closest table row that has the attribute with data-id
    window.$('body').on('click', '.regular_booking_delete', (event) => {
        if (confirm('Are you sure you want to delete this regular booking?')) {
            let $row = window.$(event.target).closest('tr'),
                $results = window.$('#body').find('#results'),
                number_of_results = parseInt($results.html().trim());
            del('regular_bookings', { _id: parseInt($row.attr('data-id')) });
            $row.remove();
            $('#results').html(number_of_results - 1);
            if (window.$('.regular_bookings_select:checked').length === 0) {
                window.$('.regular_bookings_select_all').prop('checked', false);
            }
        }
    });

    // functionality for being able to bulk select a bunch of rows, and then decide that you are wanting to bulk delete
    // a select number of rows. this functionality will iterate over all the selected checkbox items and then find
    // that checkbox row and take the data-attr('id') and utilise that as the finding for the id that we are going to
    // remove from the database.
    window.$('body').on('click', '.bulk_delete_regular_bookings', (event) => {
        if (confirm('Are you sure you want to delete these regular bookings?')) {
            var $results = window.$('#body').find('#results');
            window.$('.regular_bookings_select:checked').each((key,regular_booking_select) => {
                var number_of_results = parseInt($results.html().trim()),
                    $regular_booking_select = window.$(regular_booking_select),
                    $row = $regular_booking_select.closest('tr');
                del('regular_bookings', { _id: parseInt($row.attr('data-id')) });
                $row.remove();
                $results.html(number_of_results - 1);
            }, $results);
            window.$(event.target).addClass('uk-hidden');
            if (window.$('.regular_bookings_select:checked').length === 0) {
                window.$('.regular_bookings_select_all').prop('checked', false);
            }
        }
    });
});

/**
*
* @returns {Array}
*/
var get_regular_bookings_fields = function () {
    return [
        {
            field: 'booking_name',
            placeholder: 'Enter a name for the booking...',
            required: true,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'bookings_user',
            required: true,
            type: 'select',
            cast: 'object',
            options: get('bookings_users'),
            options_values: ['name', 'postcode']
        },
        {
            field: 'from',
            placeholder: 'Where from...',
            required: true,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'to',
            placeholder: 'Where to...',
            required: true,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'monday',
            placeholder: 'Time... 00:00 - 23:59',
            required: false,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'tuesday',
            placeholder: 'Time... 00:00 - 23:59',
            required: false,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'wednesday',
            placeholder: 'Time... 00:00 - 23:59',
            required: false,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'thursday',
            placeholder: 'Time... 00:00 - 23:59',
            required: false,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'friday',
            placeholder: 'Time... 00:00 - 23:59',
            required: false,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'saturday',
            placeholder: 'Time... 00:00 - 23:59',
            required: false,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'sunday',
            placeholder: 'Time... 00:00 - 23:59',
            required: false,
            type: 'text',
            cast: 'string'
        },
        {
            field: 'cost',
            placeholder: 'Cost £...',
            required: true,
            type: 'string',
            cast: 'number'
        }
    ];
};


/**
* This method is particularly for viewing a singular model, or if we cannot find a model, we are going to be dealing
* with a new instance of a regular bookings all together. (this method is entirely for handling the creation and editing
* of regular bookings)
*
* @param id
*/
var view_regular_booking = async function (id = null) {
    var regular_booking = get('regular_bookings', { _id: parseInt(id) }).first(),
        html = create_form('regular_booking', get_regular_bookings_fields(), regular_booking);

    window.$('#title').html(': Regular Booking: ' + print(
    	`${regular_booking.name} - ${regular_booking.when}`,
		'Create new Regular booking'
	));

    window.$('#body').html(html);

    // when we click on view regular booking, we are going to want to generate the datepickers (timepickers)
    // specifically for the entries specified in this array below, when we do, we are going to apply the datepicker
    // functionality to them.
    var date_pickers_elements = [
        '.regular_booking_monday',
        '.regular_booking_tuesday',
        '.regular_booking_wednesday',
        '.regular_booking_thursday',
        '.regular_booking_friday',
        '.regular_booking_saturday',
        '.regular_booking_sunday'
    ];

    date_pickers_elements.forEach((element, key) => {
        window.$(element).daterangepicker({
            timePicker: true,
            datepicker: false,
            singleDatePicker: true,
            autoUpdater: false,
            autoUpdateInput: false,
            locale: {
                format: 'HH:mm',
                direction: 'daterangepicker-time-only'
            },
            alwaysShowCalendars: false
        }, (date) => {
            window.$(element).val(date.format('HH:mm'));
        });
    });

    window.$('.regular_booking_bookings_user').select2();
};

/**
* This method is for returning all of the regular bookings that are lying within the system. This will
* get('regular_bookings') and iterate over them, creating a table view out of them and ensuring that the user can search
* view, edit, delete, run and more.
*
* @param search
*/
var view_regular_bookings = async function(search = null) {
	var regular_bookings = get('regular_bookings'),
		html = '';

	if (search !== null) {
	    regular_bookings = regular_bookings.filter(regular_booking =>
            (typeof (regular_booking.name) !== "undefined" && regular_booking.name.toLowerCase().includes(search.toLowerCase())) ||
            (typeof (regular_booking.from) !== "undefined" && regular_booking.from.toLowerCase().includes(search.toLowerCase())) ||
            (typeof (regular_booking.to) !== "undefined" && regular_booking.to.toLowerCase().includes(search.toLowerCase()))
        )
    }
	
    html += '<div id="regular_bookings" class="box-wrapper">';
        html += '<div class="uk-flex">';
            html += '<div class="uk-flex-auto">';
                html += '<div class="regular_bookings_options">';
                    html += '<a class="make_bookings_from_regulars uk-button uk-button-primary uk-button-small" uk-tooltip="title: Run all regular bookings; pos: right">Create regular bookings</a>';
                    html += '<a class="bulk_delete_regular_bookings uk-button uk-button-small uk-button-danger uk-hidden"><i class="fa fa-trash"></i>Delete Selected</a>';
                html += '</div>';
            html += '</div>';
            html += '<div class="uk-flex-expand">';
                html += '<input type="text" class="search regular_bookings_search" placeholder="Search..." value="' + (search !== null ? search : '') + '" />';
                html += '<a class="add regular_booking_add"><i class="fa fa-plus"></i></a>';
            html += '</div>';
        html += '</div>';
        var fields = ['booking_name', 'bookings_user.name', 'from', 'to', 'cost'];
        html += create_table('regular_booking', fields, regular_bookings);
	html += '</div>';

	window.$('#body').html(html);
    window.$('#title').html(': Regular Bookings');
    window.$('.header-navigation.active').removeClass('active');
    window.$('.header-navigation.view_regular_bookings').addClass('active');

    // if the search value has been specified then we are going to focus the search bar, so that the user can alter
    // the search parameters if they choose to do so.
    if (search !== null) window.$('.regular_bookings_search').focus();
};

/**
* this method is entirely utilised for grabbing all the regular bookings that has been set; and then the system will
* automatically calculate from this list and put them into the bookings, providing that the parameters don't already
* exist... with this; we will be able to pass in a particular regular booking id; and then calculate extra bookings
* from just one user; if they desire the entire week, month, year... interval might be relevant.
*
* @param id
* @param run_from_timeout
*/
var create_regular_bookings = async function (id = null, run_from_timeout = false) {
    var options = {};

    if (id !== null) {
        options = { _id: parseInt(id) };
    }

    // get all the regular bookings that are in the system; set the bookings made to (0) if any bookings are made
    // this will be incremented and alerted at the end of it... and also state the days that we've made, monday through
    // to sunday, and then iterate over these days over each regular booking to see if we're able to make that day
    // checking whether or not that day exists in the system and if it doesn't, make it, otherwise don't.
	var regular_bookings = get('regular_bookings', options),
        bookings_made = 0,
        days_to_check = [
            'sunday',
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday'
        ];

    regular_bookings.forEach((regular_booking, key) => {
        // every regular bookings, we are going to reset today, back to the start of the week, every time we iterate
        // over the date, we are adding on a day, this requires to be inside the iterator of the regular bookings
        // otherwise we're just going to forever keep adding days on.
        var today = new moment().startOf('week');
        // iterate over the days to check, monday - sunday.
        days_to_check.forEach((day, key) => {
            if (regular_booking[day] !== '') {
                var date_piece_1 = today.add(1, 'day').format('YYYY/MM/DD'),
                    date_piece_2 = regular_booking[day],
                    when = `${date_piece_1} @ ${date_piece_2}`,
                    entry_exists = get('bookings', {
                        bookings_user_id: regular_booking.bookings_user._id,
                        when: when
                    });

                // after iterating over the dates, and building the new date pieces then we are going to want to insert
                // this into the database, however; we are going to check whether or not this entry exists for a
                // specific name date combination and if so, then we are going to ignore it.
                if (entry_exists.length === 0) {
                    save('bookings', null, {
                        booking_name: regular_booking.booking_name,
                        name: regular_booking.name,
                        bookings_user: regular_booking.bookings_user,
                        bookings_user_id: regular_booking.bookings_user_id,
                        from: regular_booking.from,
                        to: regular_booking.to,
                        when: when,
                        cost: regular_booking.cost
                    });
                    bookings_made += 1;
                }
            }
        }, { regular_booking, today, bookings_made });
    }, { days_to_check });

    if (run_from_timeout === false) {
        alert(`${bookings_made} bookings made`);
    }
};