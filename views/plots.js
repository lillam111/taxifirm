window.$(() => {
    // when clicking on the header navigation or drivers_Back, we are going to call the method view_drivers() so that
    // the table data for bookings will display on the page.
    window.$('body').on('click', '.header-navigation.view_plots, .plot_back', (event) => {
        start_loader();
        setTimeout(() => {
            view_plots().then(() => {
                end_loader();
            });
        }, 50);
    });

    // when clicking on booking_view element in the table, we are going to call the view_driver() method, pass in the id
    // of the element we have just clicked on so that we can see that driver in detail.
    window.$('body').on('click', '.plot_view', (event) => {
        start_loader();
        setTimeout(() => {
            view_plot(window.$(event.target).closest('tr').attr('data-id')).then(() => {
                end_loader();
            });
        }, 50);
    });

    // when we click the add vehicle button, we are going to be loading a view for the viewing of a vehicle - this will
    // literally just transform the html page and enter new content into the $('#body') html tag... no id will be passed
    // so that we know that we aren't going to be working with an existing vehicle...
    window.$('body').on('click', '.plot_add', (event) => {
        start_loader();
        setTimeout(() => {
            view_plot(null).then(() => {
                end_loader();
            });
        }, 50);
    });

    // functionality for being able to toggle select and unselect all checkbox items... this will be for ease of
    // deleting entry items.
    window.$('body').on('click', '.plots_select_all', (event) => {
        let $this = window.$(event.target);
        $this.closest('table').find('.plots_select').prop('checked', $this.prop('checked'));
        // if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
        // currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
        // so that the user will be able to make global deletions
        window.$('.plots_options').find('.bulk_delete_drivers').addClass('uk-hidden');
        if (window.$('.plots_select:checked').length > 0) {
            window.$('.plots_options').find('.uk-hidden').removeClass('uk-hidden');
        }
    });

    // functionality for when clicking on the selections, and will continuously keep checking the length of the currently
    // existing checked checkboxes to see whether or not the overall checkbox (select all) wants deselecting because
    // nothing is selected...
    window.$('body').on('click', '.plots_select', (event) => {
        if (window.$('.plots_select:checked').length === 0) {
            window.$('.plots_select_all').prop('checked', false);
        }
        // if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
        // currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
        // so that the user will be able to make global deletions
        window.$('.plots_options').find('.bulk_delete_plots').addClass('uk-hidden');
        if (window.$('.plots_select:checked').length > 0) {
            window.$('.plots_options').find('.uk-hidden').removeClass('uk-hidden');
        }
    });

    // this functionality is in place so that the user is going to be able to enter some search term into the field,
    // press enter and have the drivers come back with filtered results...
    window.$('body').on('keydown', '.plots_search', (event) => {
        if (event.key === 'Enter') {
            start_loader();
            setTimeout(() => {
                view_plots(window.$(event.target).val()).then(() => {
                    end_loader();
                });
            }, 50);
        }
    });

    // when the user clicks on plot_save element... we are going to be finding the form element (which will be
    // assigned to a div grid element in the stack (for formatting purposes)) this method will be getting the fields
    // from the get fields method of this stack; and iterating over them to find the corresponding element in the stack.
    // the element will be plot_fieldname and this offers some sense of simplistic nature to the overall process.
    window.$('body').on('click', '.plot_save', (event) => {
        let $this = window.$(event.target),
            $form = $this.closest('.form'),
            plot_id = (
                typeof($this.attr('data-id')) !== "undefined" && $this.attr('data-id') !== ''
            ) ? { _id: parseInt($this.attr('data-id')) } : null;

        var plot_data = validate_form('plot', $form, get_plot_fields());

        if (! plot_data.errors) {
            // save the plot, the id will either be passed through as an object id: plot_id or will be passed as
            // null. which means that this will either be a save feature, or an edit feature. (this is combining the
            // functionality make and edit).
            save('plots', plot_id, plot_data.data);

            // after we have inserted a new plot, we are going to want to view the plots again; so that this will be
            // re-injected into the listing upon creation.
            start_loader();
            setTimeout(() => {
                view_plots().then(() => {
                    end_loader();
                });
            }, 50);
        }
    });

    // functionality for being able to delete the entry itself. this method on click of body, will latch this event onto
    // any class that has plot_delete... and will find the closest table row that has the attribute with data-id,
    // when deleting the element, we are going to simply be removing the dom element, and not refreshing the dataset
    // as when we are dealing with large amounts of data, the refresh rate of this will be intensive on the CPU.
    // we don't need to keep refreshing the table, the filtering, the ordering... only just simply remove the row
    // in question.
    window.$('body').on('click', '.plot_delete', (event) => {
        if (confirm('Are you sure you want to delete this plot?')) {
            let $row = window.$(event.target).closest('tr'),
                $results = window.$('#body').find('#results'),
                number_of_results = parseInt($results.html().trim());
            del('plots', { _id: parseInt($row.attr('data-id'))});
            $row.remove();
            $('#results').html(number_of_results - 1);
            if (window.$('.plots_select:checked').length === 0) {
                window.$('.plots_select_all').prop('checked', false);
            }
        }
    });

    // functionality for being able to bulk select a bunch of rows, and then decide that you are wanting to bulk delete
    // a select number of rows. this functionality will iterate over all the selected checkbox items and then find
    // that checkbox row and take the data-attr('id') and utilise that as the finding for the id that we are going to
    // remove from the database.
    window.$('body').on('click', '.bulk_delete_plots', (event) => {
        if (confirm('Are you sure you want to delete these plots?')) {
            var $results = window.$('#body').find('#results');
            window.$('.plots_select:checked').each((key,plot_select) => {
                var number_of_results = parseInt($results.html().trim()),
                    $plot_select = window.$(plot_select),
                    $row = $plot_select.closest('tr');
                del('plots', { _id: parseInt($row.attr('data-id')) });
                $row.remove();
                $results.html(number_of_results - 1);
            }, $results);
            window.$(event.target).addClass('uk-hidden');
            if (window.$('.plots_select:checked').length === 0) {
                window.$('.plots_select_all').prop('checked', false);
            }
        }
    });
});

/**
 * this method is for returning the field data for this overall widget in the system; this is a nice method for
 * being able to quickly generate the form elements on the page, as well as quickly generating the saving features
 * on this widget. - This method will be taking the following parameters and potentially more to come:
 * { field, placeholder, required }
 *
 * @returns {{field: string, placeholder: string, required: boolean}[]}
 */
var get_plot_fields = function () {
    return [
        {
            field: 'name',
            placeholder: 'Name...',
            required: true
        }
    ];
};

/**
 *
 * @param search
 */
var view_plots = async function (search = null) {
    var plots = get('plots'),
        html = '';

    if (search !== null) {
        plots = plots.filter(plot =>
            (typeof (plot.name) !== "undefined" && plot.name.toLowerCase().includes(search.toLowerCase()))
        );
    }

    html += '<div id="plots" class="box-wrapper">';
        html += '<div class="uk-flex">';
            html += '<div class="uk-flex-auto">';
                html += '<div class="plots_options">';
                    html += '<a class="bulk_delete_plots uk-button uk-button-small uk-button-danger uk-hidden"><i class="fa fa-trash"></i>Delete Selected</a>';
                html += '</div>';
            html += '</div>';
            html += '<div class="uk-flex-expand">';
                html +='<input type="text" class="search plots_search" placeholder="Search..." value="' + (search !== null ? search : '') + '" />';
                html +='<a class="add plot_add"><i class="fa fa-plus"></i></a>';
            html += '</div>';
        html += '</div>';
        var fields = ['name'];
        html += create_table('plot', fields, plots);
    html += '</div>';

    window.$('#body').html(html);
    window.$('#title').html(': Plots');
    window.$('.header-navigation.active').removeClass('active');
    window.$('.header-navigation.view_plots').addClass('active');

    // if the search value has been specified then we are going to focus the search bar, so that the user can alter
    // the search parameters if they choose to do so.
    if (search !== null) window.$('.plots_search').focus();
};

/**
 * This method is particularly for viewing a singular model, or if we cannot find a model, we are going to be dealing
 * with a new instance of a driver all together. (this method is entirely for handling the creation and editing of
 * drivers)
 *
 * @param id
 */
var view_plot = async function (id = null) {
    var plot = get('plots', { _id: parseInt(id) }).first(),
        html = create_form('plot', get_plot_fields(), plot);

    window.$('#title').html(print(`: Plot: ${plot.name}`, ': Create new Plot'));
    window.$('#body').html(html);
};

// ____________________________________________________________________________________________________________________
//
// Developer environment oriented functionality for what this specific segment of the application is going to need
// this entire segment of code will be entirely for testing purposes or making the testing of this application easier.
// ____________________________________________________________________________________________________________________

if (process.env.APP_MODE === 'dev') {
    var seed_plots = function() {
        save('plots', null, { 'name': 'Eckington' },      (success, message) => {});
        save('plots', null, { 'name': 'Old Wittington' }, (success, message) => {});
    };
}