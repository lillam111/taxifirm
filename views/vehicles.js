window.$(() => {
	// when clicking on the header navigation or vehicle_back, we are going to call the method view_vehicles() so that
	// the table data for vehicles will display on the page.
	window.$('body').on('click', '.header-navigation.view_vehicles, .vehicle_back', (event) => {
		start_loader();
		setTimeout(() => {
			view_vehicles().then(() => {
				end_loader();
			});
		}, 50);
	});

	// when clicking on vehicle_view element in the table, we are going to call the view_vehicle() method, pass in the
	// id of the element we have just clicked on so that we can see that booking in detail.
	window.$('body').on('click', '.vehicle_view', (event) => {
		start_loader();
		setTimeout(() => {
			view_vehicle(window.$(event.target).closest('tr').attr('data-id')).then(() => {
				end_loader();
			});
		}, 50);
	});

	// when we click the add vehicle button, we are going to be loading a view for the viewing of a vehicle - this will
	// literally just transform the html page and enter new content into the $('#body') html tag... no id will be passed
	// so that we know that we aren't going to be working with an existing vehicle...
	window.$('body').on('click', '.vehicle_add', (event) => {
		start_loader();
		setTimeout(() => {
			view_vehicle(null).then(() => {
				end_loader();
			});
		}, 50);
	});

	// functionality for being able to toggle select and unselect all checkbox items... this will be for ease of
	// deleting entry items.
	window.$('body').on('click', '.vehicles_select_all', (event) => {
		let $this = window.$(event.target);
        $this.closest('table').find('.vehicles_select').prop('checked', $this.prop('checked'));
		// if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
		// currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
		// so that the user will be able to make global deletions
		window.$('.vehicles_options').find('.bulk_delete_vehicles').addClass('uk-hidden');
		if (window.$('.vehicles_select:checked').length > 0) {
			window.$('.vehicles_options').find('.uk-hidden').removeClass('uk-hidden');
		}
	});

	// functionality for when clicking on the selections, and will continuously keep checking the length of the
	// currently existing checked checkboxes to see whether or not the overall checkbox (select all) wants deselecting
	// because nothing is selected...
	window.$('body').on('click', '.vehicles_select', (event) => {
		if (window.$('.vehicles_select:checked').length === 0) {
			window.$('.vehicles_select_all').prop('checked', false);
		}
		// if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
		// currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
		// so that the user will be able to make global deletions
		window.$('.vehicles_options').find('.bulk_delete_vehicles').addClass('uk-hidden');
		if (window.$('.vehicles_select:checked').length > 0) {
			window.$('.vehicles_options').find('.uk-hidden').removeClass('uk-hidden');
		}
	});

	// this functionality is in place so that the user is going to be able to enter some search term into the field,
	// press enter and have the vehicles come back with filtered results...
	window.$('body').on('keydown', '.vehicles_search', (event) => {
		if (event.key === 'Enter') {
			start_loader();
			setTimeout(() => {
				view_vehicles(window.$(event.target).val()).then(() => {
					end_loader();
				});
			}, 50);
		}
	});

	// when the user clicks on vehicle_save element... we are going to be finding the form element (which will be
	// assigned to a div grid element in the stack (for formatting purposes)) this method will be getting the fields
	// from the get fields method of this stack; and iterating over them to find the corresponding element in the stack.
	// the element will be vehicle_fieldname and this offers some sense of simplistic nature to the overall process.
	window.$('body').on('click', '.vehicle_save', (event) => {
		let $this = window.$(event.target),
			$form = $this.closest('.form'),
			vehicle_id = (
				typeof($this.attr('data-id')) !== "undefined" && $this.attr('data-id') !== ''
			) ? { _id: parseInt($this.attr('data-id')) } : null;

		var vehicle_data = validate_form('vehicle', $form, get_vehicle_fields());

		if (! vehicle_data.errors) {
			// save the vehicle, the id will either be passed through as an object id: vehicle_id or will be passed as
			// null. which means that this will either be a save feature, or an edit feautre. (this is cominbing the
			// functionality make and edit).
			save('vehicles', vehicle_id, vehicle_data.data);

			// after we have inserted a new vehicle, we are going to want to view the vehicles again; so that this will
			// be re-injected into the listing upon creation.
			start_loader();
			setTimeout(() => {
				view_vehicles().then(() => {
					end_loader();
				});
			}, 50);
		}
	});

	// functionality for being able to delete the entry itself. this method on click of body, will latch this event onto
	// any class that has vehicle_delete... and will find the closest table row that has the attribute with data-id
	window.$('body').on('click', '.vehicle_delete', (event) => {
		if (confirm('Are you sure you want to delete this vehicle?')) {
			let $row = window.$(event.target).closest('tr'),
				$results = window.$('#body').find('#results'),
				number_of_results = parseInt($results.html().trim());
			del('vehicles', { _id: parseInt($row.attr('data-id')) });
			$row.remove();
			$('#results').html(number_of_results - 1);
			if (window.$('.vehicles_select:checked').length === 0) {
				window.$('.vehicles_select_all').prop('checked', false);
			}
		}
	});

	// functionality for being able to bulk select a bunch of rows, and then decide that you are wanting to bulk delete
	// a select number of rows. this functionality will iterate over all the selected checkbox items and then find
	// that checkbox row and take the data-attr('id') and utilise that as the finding for the id that we are going to
	// remove from the database.
	window.$('body').on('click', '.bulk_delete_vehicles', (event) => {
		if (confirm('Are you sure you want to delete these vehicles?')) {
			var $results = window.$('#body').find('#results');
			window.$('.vehicles_select:checked').each((key,vehicle_select) => {
				var number_of_results = parseInt($results.html().trim()),
					$vehicle_select = window.$(vehicle_select),
					$row = $vehicle_select.closest('tr');
				del('vehicles', { _id: parseInt($row.attr('data-id')) });
				$row.remove();
				$results.html(number_of_results - 1);
			}, $results);
			window.$(event.target).addClass('uk-hidden');
			if (window.$('.vehicles_select:checked').length === 0) {
				window.$('.vehicles_select_all').prop('checked', false);
			}
		}
	});
});

/**
* this method is for returning the field data for this overall widget in the system; this is a nice method for being
* able to quickly generate the form elements on the page, as well as quickly generating the saving features on this
* widget. - This method will be taking the following parameters and potentially more to come:
* { field, placeholder, required }
*
* @returns {*[]}
*/
var get_vehicle_fields = function () {
	return [
		{
			field: 'name',
			placeholder: 'Name...',
			required: true
		},
		{
			field: 'registration',
			placeholder: 'Registration...',
			required: false
		}
	];
};

/**
* This method is for viewing all the vehicles in the system as a table listing. I'm contemplating using a div list so
* that this can be responsive and show specific datasets on mobile as well as desktop, this would be worth while doing
* so that I can hide spceific fields on mobile if the screen size is over a specific size. The data will still be loaded
* in however, not shown.
*
* @param search
* @returns view_vehicles
*/
var view_vehicles = async function (search = null) {
	var vehicles = get('vehicles'),
		html = '';

	if (search !== null) {
		vehicles = vehicles.filter(vehicle =>
			(typeof (vehicle.name) !== "undefined" && vehicle.name.toLowerCase().includes(search.toLowerCase())) ||
			(typeof (vehicle.registration) !== "undefined" && vehicle.registration.toLowerCase().includes(search.toLowerCase()))
		);
	}

	html += '<div id="vehicles" class="box-wrapper">';
		html += '<div class="uk-flex">';
			html += '<div class="uk-flex-auto">';
				html += '<div class="vehicles_options">';
					html += '<a class="bulk_delete_vehicles uk-button uk-button-small uk-button-danger uk-hidden"><i class="fa fa-trash"></i>Delete Selected</a>';
				html += '</div>';
			html += '</div>';
			html += '<div class="uk-flex-expand">';
				html += '<input type="text" class="search vehicles_search" placeholder="Search..." value="' + (search !== null ? search : '') + '" />';
				html += '<a class="add vehicle_add"><i class="fa fa-plus"></i></a>';
			html += '</div>';
		html += '</div>';
		var fields = ['name', 'registration'];
		html += create_table('vehicle', fields, vehicles);
	html += '</div>';

	window.$('#body').html(html);
	window.$('#title').html(': Vehicles');
	window.$('.header-navigation.active').removeClass('active');
	window.$('.header-navigation.view_vehicles').addClass('active');

	// if the search value has been specified then we are going to focus the search bar, so that the user can alter
	// the search parameters if they choose to do so.
	if (search !== null) window.$('.vehicles_search').focus();
};


/**
* This method is particularly for viewing a singular model, or if we cannot find a model, we are going to be dealing
* with a new instance of a vehicles all together. (this method is entirely for handling the creation and editing of
* Vehicles)
*
* @param id
*/
var view_vehicle = async function (id = null) {
	let vehicle = get('vehicles', { _id: parseInt(id) }).first(),
		html = create_form('vehicle', get_vehicle_fields(), vehicle);

	window.$('#title').html(print(`: Vehicle ${vehicle.name}`, ': Create new vehicle'));
	window.$('#body').html(html);
};

// ____________________________________________________________________________________________________________________
// 
// Developer environment oriented functionality for what this specific segment of the application is going to need
// this entire segment of code will be entirely for testing purposes or making the testing of this application easier.
// ____________________________________________________________________________________________________________________

if (process.env.APP_MODE === 'dev') {
	var seed_vehicles = function() {
		save('vehicles', null, { 'name': 'Car' },      (success, message) => {});
		save('vehicles', null, { 'name': 'Mini Van' }, (success, message) => {});
	}
}