window.$(() => {
    // when clicking on the header navigation or bookings_reports_back, we are going to call the method
    // view_bookings_reports() so that the table data for bookings will display on the page.
    window.$('body').on('click', '.header-navigation.view_bookings_reports, .bookings_reports_back', (event) => {
        start_loader();
        setTimeout(() => {
            view_bookings_reports().then(() => {
                end_loader();
            });
        }, 50);
    });

    // when we are changing the bookings report user id we are then going to update the table, by trying to find
    // all the elements in the get('bookings') that have the _id of the id that we're passing... this will ensure that
    // the bookings coming back are relevant.
    window.$('body').on('change', '.bookings_report_users', function (event) {
        start_loader();
        setTimeout(() => {
            view_bookings_report_bookings(
                window.$('.bookings_report_date').val(),
                window.$(this).val()
            ).then(() => {
                end_loader();
            });
        }, 50);
    });

    window.$('body').on('click', '.bookings_report_export', (event) => {
        let $this = window.$(event.target);
        if (typeof ($this.attr('href')) === "undefined" || typeof ($this.attr('download')) === "undefined") {
            alert('there is nothing to export');
        }
    });
});

/**
* This method takes care of the view of the bookings report in general, this method will attach some data to the body
* attribute that's specified in main.html; and then we're going to insert some form values so that we can interact
* with later down the line to filter down some bookings.
*
* @return window.$('body').html(return html));
*/
const view_bookings_reports = async function () {
    var html = '',
        booking_users = get('bookings_users');

    var bookings_users_options_html = '<option value="">Please select value</option>';
    booking_users.forEach((option, key) => {
        bookings_users_options_html += Html.option(option._id, option.name + ' ' + option.postcode);
    }, { bookings_users_options_html });

    html += '<div class="uk-flex form">';
        html += '<div class="uk-width-expand">';
            html += Html.input('text', 'Date...', '', 'bookings_report_date');
        html += '</div>';
        html += '<div class="uk-width-expand">';
            html += Html.select(bookings_users_options_html,'bookings_report_users');
        html += '</div>';
        html += '<div class="uk-width-auto uk-flex uk-flex-middle">';
            html += '<a class="save bookings_report_export">Export</a>';
        html += '</div>';
    html += '</div>';
    html += '<div class="uk-width-1-1 uk-margin-top">';
        html += '<div class="report_bookings"></div>';
    html += '</div>';

    window.$('#body').html(html);
    window.$('#title').html(': Bookings Reports');
    window.$('.header-navigation.active').removeClass('active');
    window.$('.header-navigation.view_bookings_reports').addClass('active');

    // initiate the booking when daterangepicker... attach an event onto this particular element as well, when we are
    // updating the date, we are going to be updating the loader to start, and then fire the view bookings report bookings
    // method which will udpate the page with the elements from this page.
    window.$('.bookings_report_date').datepicker({
        format: "yyyy/mm",
        viewMode: "months",
        minViewMode: "months"
    }).on('changeDate', function (event) {
        start_loader();
        setTimeout(() => {
            view_bookings_report_bookings(
                window.$(this).val(),
                window.$('.bookings_report_users').val()
            ).then(() => {
                end_loader();
            });
        });
    });

    window.$('.bookings_report_users').select2();
};

/**
* This method is purely for returning values from the bookings table, we are going to return all values from the table
* and then filter them down, this could potentially get slow, however we have loader feature now which looks pretty
* nifty and lets the user know that something is happening. This method is going to take a year/month
* if you don't provide one  it will bring them all back, and filter them down respectively.
*
* @param date
* @param user_id
* @returns {Promise<void>}
*/
const view_bookings_report_bookings = async function (date = null, user_id = null) {
    var bookings = get('bookings').sort((b1, b2) => moment(b1.when) - moment(b2.when));

    bookings = bookings.filter(booking =>
        booking.when.split('@')[0].includes(date)
    );

    bookings = bookings.filter(booking =>
        booking.bookings_user_id === parseInt(user_id)
    );

    if (user_id !== null && user_id !== '' && bookings.length > 0) {
        create_bookings_report(
            window.$('.bookings_report_date').val(),
            window.$('.bookings_report_users').val()
        ).then((returned_file_path) => {
            window.$('.bookings_report_export').attr('download', returned_file_path);
            window.$('.bookings_report_export').attr('href', returned_file_path);
        });
    } else {
        window.$('.bookings_report_export').removeAttr('download');
        window.$('.bookings_report_export').removeAttr('href');
    }

    var fields = ['booking_name', 'bookings_user.name', 'when', 'from', 'to', 'cost'];
    window.$('.report_bookings').html(create_table('booking', fields, bookings, false));
};

/**
* This method is for exporting the dataset that is showing on the screen; whatever the screen is displaying from the
* values that the user has inputted; this is the data that will get exported into a PDF.
*
* @param date
* @param user_id
* @returns {Promise<void>}
*/
const create_bookings_report = async function (date = null, user_id = null) {
    // remove all the existing pdfs before we make any more.
    remove_existing_pdfs();

    var bookings = get('bookings').sort((b1, b2) => moment(b1.when) - moment(b2.when)),
        booking_user = get('bookings_users', { _id: parseInt(user_id) }).first(),
        file_date = (moment().get('month') <= 9) ? '0' + (moment().get('month') + 1) : moment().get('month');
        file_date += '-';
        file_date += moment().get('year');

    if (date !== null && date !== '') {
        bookings = bookings.filter(booking =>
            booking.when.split('@')[0].includes(date)
        ); file_date = date.replace('/', '-');
    }

    if (user_id !== null && user_id !== '') {
        bookings = bookings.filter(booking =>
            booking.bookings_user_id === parseInt(user_id)
        );
    }

    // if the user id is null, then we are going to alert the user to please select a user, a user is needed in order
    // for a pdf to be generated correctly; with the appropriate fields.
    if (user_id === null || user_id === '') {
        return;
    }

    // if there are no bookings, then we are just going to return there is no bookings, and there is no point attempting
    // to continue trying to make a PDF when there aren't any results to make a PDF from.
    if (bookings.length <= 0) {
        return;
    }

    // if we do not have the exports path directory, then we are going to create it when we have executed
    // this method; this directory will need to exist in order for the application to save the pdf exports
    if (! fs.existsSync(pdf_exports_path)) {
        fs.mkdirSync(pdf_exports_path);
        fs.mkdirSync(pdf_exports_path);
    }

    // create the pdf export file path (which will include the file name
    // also set up the new PDF (document) that is following from the tutorial - pdfkit.org (shoutout)
    // var export_pdf_file_path = `${pdf_exports_path}/1.pdf`,
    var PDF = new PDFDocument,
        export_pdf_file_path = `${pdf_exports_path}/`;
        export_pdf_file_path += `${booking_user.name.replace(' ', '').toLowerCase()}`;
        export_pdf_file_path += `-${file_date}.pdf`;

    PDF.pipe(fs.createWriteStream(export_pdf_file_path));

    PDF.fontSize(18).text('Invoice', { align: 'center' });
    PDF.fontSize(10).text('Street cars, 5 Markte street, Eckington, Derbyshire, S21 4EG', { align: 'center' })
    PDF.fontSize(10).text('Tel No. 01246434373', { align: 'center' });
    PDF.fontSize(10).text('Email: streetcars@hotmail.co.uk', { align: 'center' });

    // adding a space from the titling segment of the PDF.
    PDF.moveDown();

    // For the attention of on the PDF.
    PDF.fontSize(10).text('Invoice Date ' + moment().format('L'), { align: 'right' });

    // Printing the regular booking user's name and address onto the PDF.
    PDF.fontSize(10).text(`FAO: ${booking_user.name}`);
    PDF.fontSize(10).text(`${booking_user.house_number}  ${booking_user.street}`);
    PDF.fontSize(10).text(booking_user.county);
    PDF.fontSize(10).text(booking_user.postcode);

    // the initial top and items per page...
    var row_y = 240,
        items_per_page = 21,
        row_counter = 0,
        total_price = 0,
        page = 1;

    // Taking care of the titling of the PDF fields...
    PDF.fontSize(8)
        .text('Booking name', 70, row_y)
        .text('When', 135, row_y)
        .text('Job number', 200, row_y)
        .text('From', 260, row_y)
        .text('To', 380, row_y)
        .text('Charge', 0, row_y, { align: 'right' });

    // Iterating over the PDF fields...
    bookings.forEach((booking) => {
        row_y += 20;
        var stroke_y = row_y - 6;

        PDF.fontSize(8)
            .text(booking.booking_name.elipse(12), 70, row_y)
            .text(booking.when.slice(5), 135, row_y)
            .text(booking._id, 200, row_y)
            .text(booking.from.elipse(25), 260, row_y)
            .text(booking.to.elipse(25), 380, row_y)
            .text(booking.cost.toCurrency(), 0, row_y, { align: "right" });

        PDF.strokeColor("#aaaaaa").lineWidth(1).moveTo(70, stroke_y).lineTo(540, stroke_y).stroke();

        row_counter ++;

        // if the items per page has been exceeded by the row counter, then we are going to add a new page...
        if (row_counter > items_per_page) {
            PDF.addPage();
            row_counter = 0;
            items_per_page = 30;
            row_y = 50;
            page += 1;
        }
        total_price += parseFloat(booking.cost);
    }, { PDF, row_y, items_per_page, row_counter, total_price, page });

    // Taking care of the total pricing showing on the PDF.
    PDF.moveDown();
    PDF.fontSize(8).text(`Total: ${total_price.toFixed(2).toString().toCurrency()}`, { align: 'right' });

    // end the pdf file; we are done with it... this will save the file in the chosen directory stated
    // in the requires.js
    PDF.end();

    // return the file path so that this can be referenced inside of the .then(() => {}) callback of this
    // promise.
    return export_pdf_file_path;
};

/**
*  This method will clear out the pdf exports path of all files, delete them from the system and then the system will
*  then be able to recreate them depending on the file they're looking at, this is going to be intensive, however
*  this will allow the ability to instantly download the pdf that we're looking at.
*
*  @return boolean
*/
const remove_existing_pdfs = function () {
    fs.readdir(pdf_exports_path, (err, files) => {
        if (err) throw err;
        for (const file of files) {
            fs.unlink(path.join(pdf_exports_path, file), err => {
                if (err) throw err;
            });
        }
    });

    return true;
};