window.$(() => {
	// when clicking on the header navigation or bookings_back, we are going to call the method view_bookings() so that
	// the table data for bookings will display on the page.
	window.$('body').on('click', '.header-navigation.view_bookings, .booking_back', (event) => {
		start_loader();
		setTimeout(function () {
			view_bookings().then(() => {
				end_loader();
			});
		}, 50);
	});

	// when clicking on booking_view element in the table, we are going to call the view_booking() method, pass in the
	// id of the element we have just clicked on so that we can see that booking in detail.
	window.$('body').on('click', '.booking_view', (event) => {
		start_loader();
		setTimeout(function () {
			view_booking(window.$(event.target).closest('tr').attr('data-id')).then(() => {
				end_loader();
			});
		}, 50);
	});

	// when we click the add booking button, we are going to be loading a view for the viewing of a booking - this will
	// literally just transform the html page and enter new content into the $('#body') html tag... no id will be passed
	// so that we know that we aren't going to be working with an existing booking...
	window.$('body').on('click', '.booking_add', (event) => {
		start_loader();
		setTimeout(() => {
			view_booking(null).then(() => {
				end_loader();
			});
		}, 50);
	});

	// functionality for being able to toggle select and unselect all checkbox items... this will 
	// be for ease of deleting entry items.
	window.$('body').on('click', '.bookings_select_all', (event) => {
		let $this = window.$(event.target);
        $this.closest('table').find('.bookings_select').prop('checked', $this.prop('checked'));
		// if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
		// currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
		// so that the user will be able to make global deletions
		window.$('.booking_options').find('.bulk_delete_bookings').addClass('uk-hidden');
		if (window.$('.bookings_select:checked').length > 0) {
			window.$('.booking_options').find('.uk-hidden').removeClass('uk-hidden');
		}
	});

	// functionality for when clicking on the selections, and will continuously keep checking the length
	// of the currently existing checked checkboxes to see whether or not the overall checkbox (select all)
	// wants deselecting because nothing is selected...
	window.$('body').on('click', '.bookings_select', (event) => {
		if (window.$('.bookings_select:checked').length === 0) {
			window.$('.bookings_select_all').prop('checked', false);
		}
		// if we have clicked on any of the checkboxes, then we are going to want to see if there are any that are still
		// currently checked, if that is the case, then we are going to remove the hidden class from the bulk options
		// so that the user will be able to make global deletions
		window.$('.booking_options').find('.bulk_delete_bookings').addClass('uk-hidden');
		if (window.$('.bookings_select:checked').length > 0) {
			window.$('.booking_options').find('.uk-hidden').removeClass('uk-hidden');
		}
	});	

	// this functionality is in place so that the user is going to be able to enter some search term into the field,
	// press enter and have the bookings come back with filtered results...
	window.$('body').on('keydown', '.bookings_search', (event) => {
		if (event.key === 'Enter') {
			start_loader();
			setTimeout(() => {
				view_bookings(window.$(event.target).val(), window.$('.toggle_view_bookings').hasClass('active'))
					.then(() => {
						end_loader();
					});
			}, 50);
		}
	});

	// when the user clicks on booking_save element... we are going to be finding the form element (which will be
	// assigned to a div grid element in the stack (for formatting purposes)) this method will be getting the fields
	// from the get fields method of this stack; and iterating over them to find the corresponding element in the stack.
	// the element will be booking_fieldname and this offers some sense of simplistic nature to the overall process.
	window.$('body').on('click', '.booking_save', (event) => {
		let $this = window.$(event.target),
			$form = $this.closest('.form'),
			booking_id = (
				typeof($this.attr('data-id')) !== "undefined" && $this.attr('data-id') !== ''
			) ? { _id: parseInt($this.attr('data-id')) } : null;

		var booking_data = validate_form('booking', $form, get_booking_fields());

		if (! booking_data.errors) {
			// save the booking, the id will either be passed through as an object id: booking_id or will be passed as
			// null. which means that this will either be a save feature, or an edit feature. (this is combining the
			// functionality make and edit).
			save('bookings', booking_id, booking_data.data);

			// after we have inserted a new booking, we are going to want to view the bookings again; so that this will
			// be re-injected into the listing upon creation.
			start_loader();
			setTimeout(() => {
				view_bookings().then(() => {
					end_loader();
				})
			}, 50);
		}		
	});

	// functionality for being able to delete the entry itself. this method on click of body, will latch this event onto
	// any class that has booking_delete... and will find the closest table row that has the attribute with data-id,
	// when deleting the element, we are going to simply be removing the dom element, and not refreshing the dataset
	// as when we are dealing with large amounts of data, the refresh rate of this will be intensive on the CPU.
	// we don't need to keep refreshing the table, the filtering, the ordering... only just simply remove the row
	// in question.
	window.$('body').on('click', '.booking_delete', (event) => {
		if (confirm('Are you sure you want to delete this booking?')) {
			let $row = window.$(event.target).closest('tr'),
				$results = window.$('#body').find('#results'),
				number_of_results = parseInt($results.html().trim());
			del('bookings', { _id: parseInt($row.attr('data-id')) }, true);
			$row.remove();
			$results.html(number_of_results - 1);
			if (window.$('.bookings_select:checked').length === 0) {
				window.$('.bookings_select_all').prop('checked', false);
			}
		}
	});

	// functionality for being able to bulk select a bunch of rows, and then decide that you are wanting to bulk delete
	// a select number of rows. this functionality will iterate over all the selected checkbox items and then find
	// that checkbox row and take the data-attr('id') and utilise that as the finding for the id that we are going to
	// remove from the database.
	window.$('body').on('click', '.bulk_delete_bookings', (event) => {
		if (confirm('Are you sure you want to delete these bookings?')) {
			var $results = window.$('#body').find('#results');
			window.$('.bookings_select:checked').each((key,booking_select) => {
				var number_of_results = parseInt($results.html().trim()),
					$booking_select = window.$(booking_select),
					$row = $booking_select.closest('tr');
				del('bookings', { _id: parseInt($row.attr('data-id')) });
				$row.remove();
				$results.html(number_of_results - 1);
			}, $results);
			window.$(event.target).addClass('uk-hidden');
			if (window.$('.bookings_select:checked').length === 0) {
				window.$('.bookings_select_all').prop('checked', false);
			}
		}
	});

	window.$('body').on('click', '.toggle_view_bookings', (event) => {
		let $this = window.$(event.target).prop('tagName') === 'I' ?
			window.$(event.target).parent() :
			window.$(event.target);

		start_loader();

		if (! $this.hasClass('active')) {
			setTimeout(() => {
				view_bookings(window.$('.bookings_search').val().trim(), true).then(() => {
					end_loader();
				});
			});
			return;
		}

		setTimeout(() => {
			view_bookings(window.$('.bookings_search').val().trim(), false).then(() => {
				end_loader();
			});
		}, 50);
	});

	// functionality for checking whether or not the date is blank, if it is then we are going to be adding a date on
	// ourselves. we are also going to check whether or not the input has an "@" and if not then we are going to add
	// the @ on with the time as well; some forceful validation as the time of a booking is necessary.
	window.$('body').on('blur', '.booking_when', (event) => {
		let $this = window.$(event.target);
		if (! $this.val().includes('@')) {
			var current_value = window.$(event.target).val(),
				now = moment();
			if (current_value === '') {
				current_value = now.format('YYYY/MM/DD');
			}
			window.$(event.target).val(`${current_value} @ ${now.format('HH:mm')}`);
		}
	});

	// when the user enters a date, and moves away from the date, then we are going to check to see if there are any
	// bookings on this particular day, and highlight it to the user. this will simply just show all the bookings
	// in the system for that time of the day so that the person making the booking, can check against what is already
	// in the system.
	window.$('body').on('blur', '.booking_when', (event) => {
		start_loader();
		setTimeout(() => {
			view_bookings_on_day(window.$(event.target).val().toString()).then(() => {
				end_loader();
			});
		}, 50);
	});
});

/**
* this method is for returning the field data for this overall widget in the system; this is a nice method for being
* able to quickly generate the form elements on the page, as well as quickly generating the saving features on this
* widget. - This method will be taking the following parameters and potentially more to come:
* { field, placeholder, required }
*
* @returns {*[]}
*/
var get_booking_fields = function () {
	return [
		{
			field: 'booking_name',
			placeholder: 'Enter a name for the booking...',
			required: false,
			type: 'text',
			cast: 'string'
		},
		{
			field: 'bookings_user',
			required: false,
			type: 'select',
			cast: 'object',
			options: get('bookings_users'),
			options_values: ['name', 'house_number', 'postcode']
		},
		{
			field: 'from',
			placeholder: 'Where from...',
			required: true,
			type: 'text',
			cast: 'string'
		},
		{
			field: 'to',
			placeholder: 'Where to...',
			required: true,
			type: 'text',
			cast: 'string'
		},
		{
			field: 'when',
			placeholder: 'Enter a date and time...',
			required: true,
			type: 'text',
			cast: 'date'
		},
		{
			field: 'cost',
			placeholder: 'Cost £...',
			required: true,
			type: 'text',
			cast: 'currency'
		},
		{
			field: 'driver',
			required: false,
			type: 'select',
			options: get('drivers'),
			cast: 'object',
		},
		{
			field: 'vehicle',
			required: false,
			type: 'select',
			options: get('vehicles'),
			cast: 'object',
		}
	];
};

/**
* This method is for showing all of the bookings in the system, the parameter of search value will be by default null
* and if the value isn't null, then we are going to filter out the elements that don't sit inside those items; this
* potentially is going to be a slow feature. Optimisations needed. This method will return the overall bookings in the
* system.
*
* @param search
* @param view_all
*/
var view_bookings = async function(search = null, view_all = false) {
	var bookings = get('bookings'),
		html = '';

	// take care of filtering out some elements that we aren't going to need to display on the page, this will make for
	// displaying them quicker, however the return of these elements will be a lot harsher on the CPU however needed.
	bookings = (view_all === false) ? bookings.filter(booking => moment(booking.when).isAfter(new moment())) : bookings;
	bookings = bookings.sort((b1, b2) => moment(b1.when) - moment(b2.when));

	// if the user has searched for something, then we are going to want to filter out some entries that reside within
	// the entries that we have returned from the json array... this might take some time when the system gets larger
	// depending on how big the system gets... then the user may take it upon themselves to clear out older data...
	if (search !== null) {
		bookings = bookings.filter(booking => 
			(typeof (booking.name) !== "undefined" && booking.name.toLowerCase().includes(search.toLowerCase())) ||
			(typeof (booking.from) !== "undefined" && booking.from.toLowerCase().includes(search.toLowerCase())) ||
			(typeof (booking.to) !== "undefined" && booking.to.toLowerCase().includes(search.toLowerCase())) ||
			(typeof (booking.when) !== "undefined" && booking.when.toLowerCase().includes(search.toLowerCase()))
		);
	}

    html += '<div id="bookings" class="box-wrapper">';
		html += '<div class="uk-flex">';
			html += '<div class="uk-flex-auto">';
				html += '<div class="booking_options">';
					html += '<a class="toggle_view_bookings uk-button uk-button-small uk-button-primary ' + (view_all === true ? 'active' : '') + '" uk-tooltip="title: Toggle past bookings; pos: right">';
						html += (view_all === true) ?
							'<i class="fa fa-search-minus"></i>View less' :
							'<i class="fa fa-search-plus"></i>View all';
					html += '</a>';
					html += '<a class="bulk_delete_bookings uk-button uk-button-small uk-button-danger uk-hidden"><i class="fa fa-trash"></i>Delete Selected</a>';
				html += '</div>';
			html += '</div>';
			html += '<div class="uk-flex-expand">';
				html += '<input type="text" class="search bookings_search" placeholder="Search..." value="' + (search !== null ? search : '') + '" />';
				html += '<a class="add booking_add"><i class="fa fa-plus"></i></a>';
			html += '</div>';
		html += '</div>';
		var fields = ['booking_name', 'bookings_user.name', 'when', 'from', 'to', 'cost'];
		html += create_table('booking', fields, bookings);
	html += '</div>';


	window.$('#body').html(html);
	window.$('#title').html(': Bookings');
	window.$('.header-navigation.active').removeClass('active');
	window.$('.header-navigation.view_bookings').addClass('active');

	// if the search value has been specified then we are going to focus the search bar, so that the user can alter
	// the search parameters if they choose to do so.
	if (search !== null) window.$('.bookings_search').focus();
};

/**
* This method is for passing a date in, to see what other dates exist against this one itself; however we also want
* to check whether or not an id exists on the page already, and if it does, we don't want to find the item that
* we're already looking at.
*
* @param date
*/
const view_bookings_on_day = async function (date) {
	var id = null;
	if (typeof (window.$('.booking_save').attr('data-id')) !== "undefined") {
		id = parseInt(window.$('.booking_save').attr('data-id'));
	}

	var bookings = get('bookings', { when: date }),
		fields = ['name', 'when', 'from', 'to'],
		html = '';

	if (id !== null) {
		// strip out the booking in question, if we still have elements to return, then we aren't dealing with
		// the booking we are currently looking at.
		bookings = bookings.filter(booking => booking._id !== id);
	}

	// if we have some bookings to display, then we are going to update the element with the table with entries
	// otherwise, we are just going to insert a blank string, which will remove the former elements too.
	if (bookings.length > 0) {
		html += '<div class="section">';
			html += '<h2>Existing bookings for this time</h2>';
			html += create_table('booking', fields, bookings, false);
		html += '</div>';
	}
	window.$('#bookings_existing').html(html);
};

/**
* This method is particularly for viewing a singular model, or if we cannot find a model, we are going to be dealing
* with a new instance of a booking all together. (this method is entirely for handling the creation and editing of
* bookings)
*
* @param id
*/
var view_booking = async function (id = null) {
	var booking = get('bookings', { _id: parseInt(id) }).first(),
		html = create_form('booking', get_booking_fields(), booking);

	html += '<div id="bookings_existing"></div>';

	window.$('#title').html(': Bookings: ' + print(`${booking.name} - ${booking.when}`, ' Create new booking'));
	window.$('#body').html(html);

	var date = moment(new moment(), 'YYYY/MM/DD @ HH:mm');
	if (id !== null) {
		date = moment(booking.when, 'YYYY/MM/DD @ HH:mm');
	}

	// initiate the booking when daterangepicker...
	window.$('.booking_when').daterangepicker({
		singleDatePicker: true,
		timePicker: true,
		timePicker24Hour: true,
		autoUpdateInput: false,
		startDate: date,
		local: {
			format: 'YYYY-MM-DD HH:mm',
			cancelLabel: 'Clear',
			firstDay: 1
		},
		todayHighlight: true
	}, function (date) {
		window.$('.booking_when').val(date.format('YYYY/MM/DD @ HH:mm').toString());
	});

	window.$('.booking_driver, .booking_vehicle, .booking_bookings_user').select2();
};

// ____________________________________________________________________________________________________________________
// 
// Developer environment oriented functionality for what this specific segment of the application is going to need
// this entire segment of code will be entirely for testing purposes or making the testing of this application easier.
// ____________________________________________________________________________________________________________________

if (process.env.APP_MODE === 'dev') {
	// seeding the bookings for testing purposes...
	var seed_bookings = function (seed_amount = 100) {
		var when       = ['2020/07/20 @ 10:30', '2020/07/21 @ 11:00', '2020/07/22 @ 11:30', '2020/05/23 @ 12:00', '2020/05/24 @ 12:30'],
			name       = ['Liam Taylor', 'Jarod Edwards', 'Joe Emery', 'James Prier', 'Rob Clarkson'],
			from       = ['2 Brockenhurst Road', '19 Flintham Court'],
			to         = ['2 Brockenhurst Road', '19 Flintham Court'],
			cost       = ['5.99', '6.99', '12.99', '40.00', '30.50'];

		for (i = 0; i < seed_amount; i ++) {
			save('bookings', null, {
				'booking_name': '',
				'name': name[Math.floor(Math.random() * (name.length - 1))],
				'when': when[Math.floor(Math.random() * (when.length - 1))],
				'from': from[Math.floor(Math.random() * (from.length - 1))],
				'to':   to[Math.floor(Math.random() * (to.length - 1))],
				'cost': cost[Math.floor(Math.random() * (cost.length -1))],
			}, (success, message) => {});
		}
	};
}