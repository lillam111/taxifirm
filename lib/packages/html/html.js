// html creation package, nothing fancy... 
// this constant and methods inside is pretty much all for creating simple ease of use html entities. 
// this will be for simplistic returning of html rather than having to completely write out lines and lines of 
// html inside javascipt... this method is for making life simpler when it comes to html inside javascript.

/**
* This constant html is utilised for ease of access html snippets which make the creation of html inside the javascript
* files for representations of views inside the application. Rather than continuously writing strenous amounts of html
* inside the javascript and making concatination strings and more; this just simplifies the process and the need for
* overused concats (even though this method is concatenating a lot) irony I know.
*
* @type {{
* div: (function(*, *, *=): string),
* input: (function(*, *=, *=, *=, *=): string),
* a: (function(*, *=, *=): string),
* label: (function(*, *=, *=): string)
* }}
*/
const Html = {
	div: function (text, classes, extra = '') {
		return '<div class="' + classes + '" ' + extra + '>' + text + '</div>';
	},

	input: function (type, placeholder = '', value = '', classes = '', extra = '') {
		if (type === 'checkbox') classes += ' uk-checkbox';
		return '<input type="' + type + '" class="' + classes + '" placeholder="' + placeholder + '" value="' + value + '" ' + extra + ' />';
	},

	select: function (options, classes = '') {
		var html = '<select class="' + classes + '">';
			html += options;
		html += '</select>';
		return html;
	},

	option: function (key, value) {
		return '<option value="' + key + '">' + value + '</option>';
	},

	a: function (text, classes = '', extra = '') {
		return '<a class="' + classes + '" ' + extra + '>' + text + '</a>';
	},

	label: function (text, classes = '', extra = '') {
		return '<label class="' + classes + '" ' + extra + '>' + text + '</label>';
	}
};

/**
* this method is entirely made for printing - if the value contains undefined in any way then we are going to either
* return a blank string, or a specifie default value.
*
* @param value
* @param default_value
* @returns {*}
*/
const print = function(value, default_value = '') {
    if (
        typeof (value) === "undefined" || (
            typeof (value) === "string" &&
            value.includes('undefined')
        )) {
        return default_value;
    } return value;
};

/**
 * UCWords function; this method is for returning the first value as an uppercase letter
 *
 * @returns {string}
 */
String.prototype.ucwords = function() {
    string = this.toLowerCase();
    return string.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g, function(character){
		return character.toUpperCase();
	});
};

/**
* Pluralise function; this method is for returning the pluralisations of texts...
*
* @returns {string}
*/
String.prototype.pluralise = function () {
	return this + 's';
};

/**
* This method returns the string back as a currency, basically will just return the value of whatever is
* being passed via the prototype, to be prepended a currency symbol.
* £this for example
*
* @param code
* @returns {string}
*/
String.prototype.toCurrency = function (code = 'gbp') {
	var currency_codes = {
		'gbp': '£'
	};
	return currency_codes[code] + this;
};

/**
* This method is to utilise the self string treat it as an arry get the limit + 1 explode and take the first set
* so that we can then add a + ... elipse; if the limit is the length of the text that's showing, then we're just
* going to return the original string
*
* @param limit
* @returns {string}
*/
String.prototype.elipse = function (limit) {
	if (this.length === limit || this.length < limit) return this;
	return this.substr(0, limit) + '...';
};

/**
* This method is particularly for creating pagination from array items, based on the number of items per page that
* will be specified inside of the method calling this function; this will take the total number of items / items per
* page and then iterate over the pages and generate html from this so that the user will be able to click on
* next/previous 1-2-3-4-5-6 etc and get taken to the page in question; we'll be using array.slice to trim off the excess
* array elements that aren't needed for the current page.
*
* @param items
* @param page
* @returns {{pagination: {total_items: number, items: *[], html: string}}}
*/
Array.prototype.paginate = function (items, page) {
	var array = this,
		pages = Math.ceil(this.length / items),
		current_page = page,
		html = '';

	html += '<div class="pagination">';
	// html += '<a class="pagination_item previous" data-page="' + page + '" ' + (page === 1) ? 'disabled' : '' + '><i class="fa fa-backward"></i>Prev</a>';
	for (i = 0; i < pages; i++) {
		html += '<a class="pagination_item ' + (current_page === (i + 1) ? 'active' : '') + '" data-page="' + (i + 1) + '">' + (i + 1) + '</a>';
	}
    // html += '<a class="pagination_item next" data-page="' + page + '" ' + (page === pages) ? 'disabled' : '' + '><i class="fa fa-forward"></i>Next</a>';
	html += '</div>';

	return {
		pagination: {
            total_items: array.length,
            items: array.slice((page - 1) * items, page * items),
            html: html,
		}
	};
};

/**
* This function is entirely for returning the first element inside of an array; ideally you are only going to want
* to return a single result when utilising this, and this is mostly going to be used within the get/getArchives function
* inside database.js... the reason for this function is so that I can easily pull out the first returned value, without
* any awkwardness inside the methods that are utilising single needed datasets.
*
* @returns {{}}
*/
Array.prototype.first = function () {
	return this.length === 0 ? {} : this[0];
};

/**
* This method is to take an object, along with a path, and return the element that's inside the object; if it cannot be
* found, then we are just going to print an empty string.
*
* @param path
* @param object
* @returns {Object|Window|WorkerGlobalScope}
*/
const resolve = (path, object) => {
	return path.split('.').reduce(function(previous, current) {
		return previous ? previous[current] : ''
	}, object || self)
};

/**
 * return html append - this function is entirely for initiating a loading screen, so that the user is in knowledge
 * that something is happening. This is a form of feedback that something systematically is going on - this will
 * prevent users from being able to do anything else whilst the system is taking care of something.
 *
 * @return html.append()
 */
const start_loader = function () {
	var html = '';
	html += '<div class="loader uk-flex uk-flex-middle uk-flex-center">';
		html += '<i class="fa fa-spin fa-spinner"></i>';
	html += '</div>';
	window.$('body').append(html);
};

/**
 * This method is a functionality that counteracts the above method (start_loader()) - this will remove the loading
 * when we know that the system is ready to do something.
 *
 * @return html.remove()
 */
const end_loader = async function () {
	window.$('.loader').remove();
};