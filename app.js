window.$(() => {
	// if the user clicks anywhere on the body... then we are going to want to fire this event, this event is literally
	// just going to remove the active and open on the corresponding elements that sit inside the click capture.
	window.$('body').on('click', function (event) {
		window.$('.menu_button').removeClass('active');
		window.$('.menu_dropdown').removeClass('open');
	});

	// when the user clicks on the menu_button we are going to stop propagation, basically the above method, we are
	// clicking inside the body; so we are going to stop the propagation and preventing from the classes instantly being
	// removed the second that they're put on; the active on the button will give the button a sense of active... as well
	// as the menu dropdown having the class of open which will have styles assigned to make it show on the screen.
	window.$('body').on('click', '.menu_button', function (event) {
		event.stopPropagation();
		let $this = window.$(this);
		$this.next().toggleClass('open');
		$this.toggleClass('active');
	});
});

const ipcRenderer = require('electron').ipcRenderer;

// wait for an updateReady message
ipcRenderer.on('updateReady', function(event, text) {
    // changes the text of the button
    var container = document.getElementById('ready');
    container.innerHTML = "new version ready!";
});

/**
*
* @param table
* @param $form
* @param fields
* @returns {{data: *, errors: *}}
*/
const validate_form = function (table, $form, fields) {
	var form_data = {},
		errors = false;

	// check over the current error messages and making sure that we're removing them; this will be to recalibrate
	// whether or not there are any further errors to amend rather than displaying the same errors, and re-injecting
	// the same error message on dates.
	window.$('.error').removeClass('error');
	window.$('.error_message').remove();

	// iterate over the fields... check the fields against what is against the field items.
	fields.forEach((field, key) => {
		form_data[field.field] = $form.find(`.${table}_${field.field}`).val();
		var $field_item = $form.find(`.${table}_${field.field}`);

		if (field.cast === 'object') {
            // checking whether or not the object we're looking at is required or not and if it is then we are going
            // to be printing errors, we return here by default, as if we do not, then we are subject to erroring.
            if (field.required === true && form_data[field.field].trim() === '') {
                $field_item.parent().parent().addClass('error');
                $field_item.parent().append('<span class="error_message">This field is required</span>');
                errors = true;
            }
		    form_data[field.field] = get(field.field.pluralise(), { _id: parseInt($field_item.val())}).first();
		    // append on the field id for the object, in order to use by reference.
		    form_data[`${field.field}_id`] = form_data[field.field]._id;
		    // perform some other validation, but we don't want to do the below checks...
		    return;
        }

		// custom validation, making sure that if the item in question, if has been specified as required that
		// we are checking the item itself to see whether or not this has any content inside of it, if it doesn't
		// then we return an error if so.
		if (field.required === true && (
            (typeof (form_data[field.field]) === "undefined" || (form_data[field.field].trim() === ''))
		)) {
			$field_item.parent().parent().addClass('error');
			$field_item.parent().append('<span class="error_message">This field is required</span>');
			errors = true;
			return;
		}

		// custom validation, making sure that we are going to be pushing up a validate date... and if not, then we
		// are going to spit an error back to the user again, and potentially some other validation with it.
		if (
			field.required === true &&
			field.cast === 'date' &&
			! moment(form_data[field.field]).isValid()
		) {
			$field_item.parent().parent().addClass('error');
			$field_item.parent().append('<span class="error_message">Please enter a valid date</span>');
			errors = true;
			return;
		}

		if (
			field.required === true &&
			field.cast === 'number' &&
			(! parseInt(form_data[field.field]) > 0 || ! parseFloat(form_data[field.field]) > 0)
		) {
			$field_item.parent().parent().addClass('error');
			$field_item.parent().append('<span class="error_message">Please enter a valid number I.E: 12, 12.99</span>');
			errors = true;
		}
	}, { form_data, $form, errors });

	return {
		errors: errors,
		data: form_data
	};
};

/**
* This method is for creating forms and a simple way of consolidating all of the information gathering into one
* globalised section of the code. All form creations will run through this method, meaning anything that happens
* to one form, happens to them all. This method is going to help for simplifying the amount of code needed
* across the overall platform.
*
* @param prefix
* @param fields
* @param object
* @returns {string}
*/
const create_form = function (prefix, fields, object = {}) {
    var html = '';
    html += '<div class="uk-grid uk-grid-small form" uk-grid>';
        fields.forEach((field, key) => {
            html += '<div class="uk-width-1-1 uk-flex" style="position: relative">';
                html += Html.label(field.field.ucwords().replace('_', ' '), 'label-flex');
                html += '<div class="uk-width-1-1">';
                    if (field.type !== 'select') {
                        html += Html.input(field.type,
                            field.placeholder,
                            print(object[field.field]),
                            `${prefix}_${field.field}`
                        );
                    }
                    if (field.type === 'select') {
                        var options_html = '<option value="">Please select value</option>';
                        field.options.forEach((option, key) => {
                            var option_value = option.name;
                            if (typeof (field.options_values) !== "undefined" && field.options_values.length > 0) {
                                option_value = '';
                                field.options_values.forEach((the_value, key) => {
                                    option_value = option_value + (key !== 0 ? ' ' : '') + option[the_value];
                                }, { option_value, option });
                            }

                            if (typeof (object[field.field]) !== "undefined" && option._id === object[field.field]._id) {
                                options_html += Html.option(option._id, option_value).replace('<option', '<option selected');
                            } else {
                                options_html += Html.option(option._id, option_value);
                            }
                        }, { options_html, object });
                        html += Html.select(options_html, `${prefix}_${field.field}`);
                    }
                html += '</div>';
            html += '</div>';
        }, html);
        html += '<div class="uk-width-1-1 uk-width-1-1 uk-flex">';
        html += Html.label('', 'label-flex');
            html += Html.div(
                Html.a('Save', `save ${prefix}_save`, 'data-id="' + print(object._id) + '"') +
                    Html.a('Back', `back ${prefix}_back`),
            'uk-width-1-1');
        html += '</div>';
    html += '</div>';
    return html;
};

/**
* Method for creating the viewing tables of datasets... this method is what will be used for displaying all of the
* information that resides inside the system.
*
* @param prefix
* @param fields
* @param objects
* @param options
* @returns {string}
*/
const create_table = function (prefix, fields, objects, options = true) {
    var html = '';
    html += '<table class="uk-table">';
        html += '<thead>';
            html += '<tr>';
                html += (options === true) ? '<th style="width: 10px;"><input type="checkbox" class="uk-checkbox ' + prefix.pluralise() + '_select_all" /></th>' : '';
                fields.forEach((field, key) => {
                    html += `<th>${field.ucwords().split('.').first().replace('_', ' ')}</th>`;
                }, html);
                html += (options === true) ? '<th></th>' : '';
            html += '</tr>';
        html += '</thead>';
        html += '<tbody>';
            if (typeof (objects) === "object" && objects.length > 0) {
                objects.forEach((object, key) => {
                    html += '<tr data-id="' + object._id + '">';
                        html += (options === true) ? '<td><input type="checkbox" class="uk-checkbox ' + prefix.pluralise() + '_select" /></td>' : '';
                        fields.forEach((field, key) => {
                            html += '<td>';
                                html += (field === 'cost') ?
                                    print(resolve(field, object)).toCurrency() : print(resolve(field, object));
                            html += '</td>';
                        }, { html, object });
                        if (options === true) {
                            html += '<td class="uk-text-right">';
                                html += '<a class="view ' + prefix + '_view" uk-tooltip="title: View/Edit"><i class="fa fa-search"></i></a>';
                                // this needs to be better...
                                if (prefix === 'regular_booking') {
                                    html += '<a class="view run_regular_bookings_create" uk-tooltip="title: Run"><i class="fa fa-refresh"></i></a>';
                                }
                                html += '<a data-toggle="confirmation" class="delete ' + prefix + '_delete" uk-tooltip="title: Delete"><i class="fa fa-trash"></i></a>';
                            html += '</td>';
                        }
                    html += '</tr>';
                }, { html, prefix });
            }
            if (objects.length === 0) {
                html += '<tr>';
                    html += '<td colspan="10">No Data Found</td>';
                html += '</tr>';
            }
        html += '</tbody>';
    html += '</table>';
    html += '<span class="info"><span id="results">' + objects.length + '</span> Result' + (objects.length > 1 ? 's' : '') + '</span>';
    return html;
};

/**
*
* @param file
* @param html
*/
const create_pdf = function (file = 'template.pdf', html = '') {
    // var options = { format: 'Letter' };
    // pdf.create(html, options).toFile(file, (err, res) => {
    //     if (err) return console.log(err);
    //     console.log(res);
    // });
};

if (process.env.APP_MODE === 'dev') {
    /**
    * this method is particualrly not really important for production... however this will just seed the application
    * with data that is specified inside the respective methods... they will also only ever be called if they have been
    * defined; otherwise this will just get iterated over. This method is just a predefined method in order to be able
    * to see the application; this will not run on load.
    */
    const seed_application = function() {
        if (typeof (seed_drivers) !== "undefined" && get('drivers').length === 0) seed_drivers();
        if (typeof (seed_vehicles) !== "undefined" && get('vehicles').length === 0) seed_vehicles();
        if (typeof (seed_bookings) !== "undefined" && get('bookings').length === 0) seed_bookings();
    };

    /**
    * This method is entirely for clearing the application, if you are needing to wipe data from this application,
    * this is the method to call, this might want wrapping at some point in a (if env === dev) otherwise this could
    * potentially be ran at some point accidentally in a console or something... this is entirely a developer tool
    * function.
    */
    const clear_application = function() {
        clear(tables);
    };
}

// initiate the first page loading... this is currently defined as the first function that should load, as this is the
// one thing that we're wanting to be the active first loading page on the application... whichever view wants to be
// shown first is what wants to load here first...
start_loader();
if (typeof (view_bookings) !== "undefined" && typeof (window.view) === "undefined") {
	window.view = view_bookings;
}
setTimeout(() => {
    window.view().then(() => {
        end_loader();
    });
}, 50);

// setting up the functionality for scheduling items, if items exist inside the bookings, then we are going to run
// then all, this will continuously keep iterating over and over, depending on the set timeout; this recursive setTimeout
// will keep going until the system is shutdown.
setInterval(function run() {
    // every 10 minutes, we are going to run this task, this will run every time that the application loads; this will
    // need commenting out if they are confused as to why certain things keep getting remade when they are deleting
    // things...
    create_regular_bookings(null, true);
}, 600000);